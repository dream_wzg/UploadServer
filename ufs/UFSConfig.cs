﻿using System;
using System.Collections.Generic;

namespace ufs
{
    public class UFSConfig
    {
        public UFSConfig()
        {
        }
        /// <summary>
        /// 允许上传的后缀名
        /// </summary>
        /// <value>The allow exts.</value>
        public HashSet<string> AllowExts { get; set; }
        /// <summary>
        /// 限制大小
        /// </summary>
        /// <value>The size of the limit.</value>
        public int? LimitSize { get; set; }
        /// <summary>
        /// 访问token
        /// </summary>
        /// <value>The access token.</value>
        public string AccessToken { get; set; }

        /// <summary>
        /// 下游上传地址配置
        /// </summary>
        public List<string> Downstreams { get; set; }
    }

}
